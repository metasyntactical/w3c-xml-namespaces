metasyntactical/w3c-xml-namespaces
==================================

This library contains several useful xsd schemas defined by the W3C to
be able to validate against the schema definitions without requiring
access to the internet.

## Installation

Open a command console, enter your project directory and execute the
following command to download the lates stable version of this bundle:

```console
$ composer require metasyntactical/w3c-xml-namespaces
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.
