#!/usr/bin/env bash

EXEC_DATE=$(date +%Y-%m-%dT%H:%M:%S)

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

URL=$1

TARGET_DIR=$DIR/src/Resources/schema

TMP_DIR=$(mktemp -d "${TMPDIR:-/tmp/}$(basename $0).XXXXXXXXXXXX")

# $1 is the URL to be downloaded from
function download_schema()
{
    local URL=$1
    local TMP_FILE=$TMP_DIR/${URL##*/}

    curl -sSL -o "$TMP_FILE" "$URL"

    local XML_NAMESPACE=$(grep "targetNamespace=\"" "$TMP_FILE" | awk -F'"' '{ print $2 }')
    local XML_SHA256=$(echo -n $XML_NAMESPACE | openssl dgst -sha256)

    local TARGET_FILE=$TARGET_DIR/$XML_SHA256

    mkdir -p "$TARGET_DIR"

    echo -n "" > "$TARGET_FILE.txt"
    echo "XML_NAMESPACE=\"$XML_NAMESPACE\"" >> "$TARGET_FILE.txt"
    echo "DOWNLOAD_URL=\"$URL\"" >> "$TARGET_FILE.txt"
    echo "DOWNLOAD_DATE=\"$EXEC_DATE\"" >> "$TARGET_FILE.txt"
    mv "$TMP_FILE" "$TARGET_FILE.xsd"

    echo -n "$XML_SHA256.xsd"
}

_=$(download_schema "$URL")
