# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]


## [1.0.0] - 2017-04-27

### Added

- Generic library to locate namespace schema by namespace name.
- XML Schema files for XMLSchema Definition and XML itself.

[Unreleased]: https://gitlab.com/metasyntactical/w3c-xml-namespaces/compare/v1.0.0...master
[1.0.0]: https://gitlab.com/metasyntactical/w3c-xml-namespaces/compare/v1.0.0...v1.0.0
