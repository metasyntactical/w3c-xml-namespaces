<?php

namespace Metasyntactical\Data\XmlNamespaces\W3C;

use DOMDocument;
use InvalidArgumentException;

final class NamespaceLocator
{
    private static $schemaDir = __DIR__.'/Resources/schema/';

    public static function locate($xmlNamespace): string
    {
        $namespaceHash = hash("sha256", $xmlNamespace);

        if (!file_exists(self::$schemaDir.$namespaceHash.'.xsd')) {
            throw new InvalidArgumentException('Unknown namespace: '.$xmlNamespace);
        }

        return self::$schemaDir.$namespaceHash.'.xsd';
    }
}
