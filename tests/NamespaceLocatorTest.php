<?php

namespace Metasyntactical\Data\XmlNamespaces\W3C;

use DOMDocument;
use PHPUnit\Framework\TestCase;

class NamespaceLocatorTest extends TestCase
{
    /**
     * @dataProvider provideValidData
     */
    public function testSchemaWillBeLoaded($xmlNamespace, $exampleDocument = null)
    {
        $actual = NamespaceLocator::locate($xmlNamespace);

        self::assertFileExists($actual);

        if (!is_null($exampleDocument)) {
            $example = new DOMDocument();
            $example->loadXML($exampleDocument);
            self::assertTrue($example->schemaValidate($actual));
        }
    }

    public function provideValidData()
    {
        return [
            'XMLSchema' => [
                'http://www.w3.org/2001/XMLSchema',
                '<?xml version="1.0" encoding="UTF-8"?><xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
                    xmlns:tns="http://example.com/example-namespace"
                    elementFormDefault="qualified" xml:lang="EN"
                    targetNamespace="http://example.com/example-namespace"
                    version="1.0"></xs:schema>',
            ],
            'XML' => [
                'http://www.w3.org/XML/1998/namespace',
            ]
        ];
    }
}
